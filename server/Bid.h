//
// Created by developer on 5/24/17.
//

#ifndef PV_BOOSTAUCTIONS_BID_H
#define PV_BOOSTAUCTIONS_BID_H


#include <boost/date_time/posix_time/ptime.hpp>
#include "Bidder.h"

namespace asioAuctions {

    class Bid {
    public:
        Bid(const boost::posix_time::ptime &m_timestamp, const std::string &client, int m_amount);

        const boost::posix_time::ptime &getM_timestamp() const;

        const std::string &getClient() const;

        int getM_amount() const;

        friend std::ostream &operator<<(std::ostream &os, const Bid &b);

    private:
//        int m_id;
        boost::posix_time::ptime m_timestamp;
//        Bidder m_bidder;
        std::string m_client;
        int m_amount;

    };



}
#endif //PV_BOOSTAUCTIONS_BID_H
