#include <boost/bind.hpp>
#include <boost/asio.hpp>
#include <boost/shared_ptr.hpp>
#include <boost/enable_shared_from_this.hpp>
#include <iostream>
#include "Auction.h"

using namespace boost::asio;
using namespace boost::posix_time;

io_service service;

// used for async_wait, x = handlerfunc
#define MEM_FN(x)       boost::bind(&self_type::x, shared_from_this())

// used for async_connect, x = endpoint, y = handlerfunc
#define MEM_FN1(x, y)    boost::bind(&self_type::x, shared_from_this(),y)

// used for callback methods on async_read/write. x = handlerfunc, y = error_code, z = number of bytes to read/write
#define MEM_FN2(x, y, z)  boost::bind(&self_type::x, shared_from_this(),y,z)

namespace asioAuctions {

    class clientManager;

    typedef boost::shared_ptr<clientManager> client_ptr;
    typedef std::vector<client_ptr> array;
    array clients;

    typedef boost::shared_ptr<Auction> auction_ptr;
    typedef std::vector<auction_ptr> auction_arr;
    auction_arr auctions;

    void update_clients_changed();

/** simple connection to server:
    - logs in just with username (no password)
    - all connections are initiated by the client: client asks, server answers
    - server disconnects any client that hasn't pinged for 5 seconds

    Possible client requests:
    - gets a list of all connected clients
    - ping: the server answers either with "ping ok" or "ping client_list_changed"
*/
    class clientManager : public boost::enable_shared_from_this<clientManager>, boost::noncopyable {
        typedef clientManager self_type;

        clientManager() : sock_(service), started_(false), timer_(service), clients_changed_(false) {}

    public:
        typedef boost::system::error_code error_code;
        typedef boost::shared_ptr<clientManager> ptr;

        void start() {
            started_ = true;
            clients.push_back(shared_from_this());
            last_ping = boost::posix_time::microsec_clock::local_time();
            // first, we wait for m_client to login
            do_read();
        }

        static ptr new_() {
            ptr new_(new clientManager);
            return new_;
        }

        void stop() {
            if (!started_) return;
            started_ = false;
            sock_.close();

            ptr self = shared_from_this();
            array::iterator it = std::find(clients.begin(), clients.end(), self);
            clients.erase(it);
            update_clients_changed();
        }

        bool started() const { return started_; }

        ip::tcp::socket &sock() { return sock_; }

        std::string username() const { return username_; }

        void set_clients_changed() { clients_changed_ = true; }

    private:
        void on_read(const error_code &err, size_t bytes) {
            if (err) stop();
            if (!started()) return;
            // process the msg
            std::string msg(read_buffer_, bytes);
            if (msg.find("login ") == 0) on_login(msg);
            else if (msg.find("ping") == 0) on_ping();
            else if (msg.find("ask_clients") == 0) on_clients();
            else if (msg.find("ask_auctions") == 0) on_auctions();
            else if (msg.find("get_auction ") == 0) on_auction_by_title(msg);
            else if (msg.find("place_bid ") == 0) on_place_bid(msg);
            else std::cerr << "invalid msg " << msg << std::endl;
        }

        void on_login(const std::string &msg) {
            std::istringstream in(msg);
            in >> username_ >> username_;
            std::cout << username_ << " logged in" << std::endl;
            do_write("login ok\n");
            update_clients_changed();
        }

        // TODO when m_client pings, answer with list of subscribed auctions
        void on_ping() {
            std::string answer("ping ");
            answer.append(clients_changed_ ? "client_list_changed" : "ok");
            do_write(answer + "\n");
            clients_changed_ = false;
        }

        void on_clients() {
            std::string answer("clients");
            for (array::const_iterator b = clients.begin(), e = clients.end(); b != e; ++b) {
                answer.append((*b)->username() + " ");
            }
            do_write(answer + "\n");
        }

        void on_place_bid(const std::string &msg) {

            std::istringstream in(msg);
            std::string title_enc;
            std::string title;
            std::string offer;
            // check for second word
            in >> title_enc >> title_enc >> offer;
            title = title_enc.substr(3);

            std::stringstream auction_ss;
            std::string answer = "bid_answer ";
            std::cout << username_ << "\tsent offer for\t'" << title << "'\twith " << offer.substr(6) << "\n";

            // TODO find problem, probably how bid-objects are managed
//            bids.emplace_back(new Bid(boost::posix_time::microsec_clock::local_time(), username_,
//                                      std::stoi(offer.substr(6))));


            for (auction_arr::const_iterator b = auctions.begin(), e = auctions.end(); b != e; ++b) {
                // TODO STL function?
                std::cout << "DEBUG: " << (*b)->getM_title() << "\n";
                if (title == (*b)->getM_title()) {
                    std::cout << "setting bid" << "\n";
//                    bool sucess = (*b)->enter_new_bid(*(bids[bids.size()-1].get()));
//                    if (sucess) {
//                        answer += "SUCESS new highest bid " + (*b)->getHighest().getM_amount();
//                    } else {
//                        answer += "FAILED highest bid is " + std::to_string((*b)->getHighest().getM_amount()) +
//                                  " your offer was " + offer.substr(6);
//                    }
//                    std::cout << "DEBUG: " << answer << "\n";
//                    auction_ss << (*b->get());
//                    answer += auction_ss.str();
                }
            }
            do_write(answer + "\n");
        }

        void on_auction_by_title(const std::string &msg) {

            std::istringstream in(msg);
            std::string title;
            // check for second word
            in >> title >> title;

            std::string answer = "get_auction ";
            std::cout << username_ << " asked for " << title << "\n";
            std::stringstream auction_ss;
            bool done = false;
            for (auction_arr::const_iterator b = auctions.begin(), e = auctions.end(); b != e; ++b) {
//                std::cout << "checking .. " << (*b)->getM_title() << " for " << username_ << "\n";
                if ((*b)->getM_title() == title) {
                    auction_ss << *b->get();
                    answer += auction_ss.str();
                    done = true;
                    std::cout << "found - answering: " << answer << "\n";
                    do_write(answer + "\n");
//                    return;
                }
            }
            if (!done) {
                std::cout << " NO_AUCTION_FOR  " << title << " answering: " << answer << "\n";
                do_write(answer + " NO_AUCTION_FOR " + title + "\n");
                return;
            }
        }


        void on_auctions() {
            std::string msg = "auctions ";
            for (auction_arr::const_iterator b = auctions.begin(), e = auctions.end(); b != e; ++b) {
                msg += (*b)->getM_title() + ",";
            }
            do_write(msg + "\n");
        }

        void do_ping() {
            do_write("ping\n");
        }

        void do_ask_clients() {
            do_write("ask_clients\n");
        }

        void on_check_ping() {
            boost::posix_time::ptime now = boost::posix_time::microsec_clock::local_time();
            if ((now - last_ping).total_milliseconds() > 5000) {
                std::cout << "stopping " << username_ << " - no ping in time" << std::endl;
                stop();
            }
            last_ping = boost::posix_time::microsec_clock::local_time();
        }

        void post_check_ping() {
            timer_.expires_from_now(boost::posix_time::millisec(5000));
            timer_.async_wait(MEM_FN(on_check_ping));
        }


        // ------------------------------
        // H E L P E R  F U N C T I O N S
        void on_write(const error_code &err, size_t bytes) {
            do_read();
        }

        void do_read() {
            async_read(sock_, buffer(read_buffer_), MEM_FN2(read_complete, _1, _2), MEM_FN2(on_read, _1, _2));
            post_check_ping();
        }

        void do_write(const std::string &msg) {
            if (!started()) return;
            std::copy(msg.begin(), msg.end(), write_buffer_);
            sock_.async_write_some(buffer(write_buffer_, msg.size()), MEM_FN2(on_write, _1, _2));
        }

        size_t read_complete(const boost::system::error_code &err, size_t bytes) {
            if (err) return 0;
            bool found = std::find(read_buffer_, read_buffer_ + bytes, '\n') < read_buffer_ + bytes;
            // we read one-by-one until we get to enter, no buffering
            return found ? 0 : 1;
        }

    private:
        ip::tcp::socket sock_;
        enum {
            max_msg = 2048
        };
        char read_buffer_[max_msg];
        char write_buffer_[max_msg];
        bool started_;
        std::string username_;
        deadline_timer timer_;
        boost::posix_time::ptime last_ping;
        bool clients_changed_;
    };

    void update_clients_changed() {
        for (array::iterator b = clients.begin(), e = clients.end(); b != e; ++b) {
            (*b)->set_clients_changed();
        }
    }

}


ip::tcp::acceptor acceptor(service, ip::tcp::endpoint(ip::tcp::v4(), 8001));

void handle_accept(asioAuctions::clientManager::ptr client, const boost::system::error_code &err) {
    client->start();
    asioAuctions::clientManager::ptr new_client = asioAuctions::clientManager::new_();
    acceptor.async_accept(new_client->sock(), boost::bind(handle_accept, new_client, _1));
}

// starts auction server with some predeclared auctions
int main(int argc, char *argv[]) {
    using namespace boost::posix_time;

    std::cout << "Starting server... " << std::endl;
    boost::posix_time::ptime now = boost::posix_time::microsec_clock::local_time();

    asioAuctions::auctions.emplace_back(
            new asioAuctions::Auction{"NashePivo", "beer 1", now + seconds(rand() % 30000)});
    asioAuctions::auctions.emplace_back(new asioAuctions::Auction{"Vivat", "kg 2", now + seconds(rand() % 30000)});
    asioAuctions::auctions.emplace_back(new asioAuctions::Auction{"Baltica", "beer 1", now + seconds(rand() % 30000)});

    asioAuctions::clientManager::ptr client = asioAuctions::clientManager::new_();
    acceptor.async_accept(client->sock(), boost::bind(handle_accept, client, _1));
    std::cout << "waiting for async accept..." << std::endl;
    service.run();
}
