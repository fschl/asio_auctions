//
// Created by fschl on 5/24/17.
//

#include "Auction.h"
#include <stdio.h>

namespace asioAuctions {

    Auction::Auction(std::string title,
                     std::string seller,
                     boost::posix_time::ptime endtime) : m_title{title},
                                                         m_seller_name{seller},
                                                         m_end{endtime},
                                                         m_highest{Bid(boost::posix_time::microsec_clock::local_time(),
                                                                       "nobody", 0)} {}

    Auction::Auction(boost::posix_time::ptime endtime) : m_end{endtime},
                                                         m_highest{Bid(boost::posix_time::microsec_clock::local_time(),
                                                                       "nobody", 0)} {
        m_title = "default Title";
        m_seller_name = "default seller";;
    }

    const std::string &Auction::getM_seller_name() const {
        return m_seller_name;
    }

    const std::string &Auction::getM_title() const {
        return m_title;
    }

    const boost::posix_time::ptime &Auction::getM_end() const {
        return m_end;
    }

    const Bid &Auction::getHighest() const {
        return m_highest;
    }

    const bool &Auction::enter_new_bid(const Bid &newOffer) {
        if (newOffer.getM_amount() < m_highest.getM_amount()) {
            std::cout << "AUCTION: bid on " << m_title << " failed with " << m_highest.getM_amount();
            return false;
        } else {
            m_highest = newOffer;
            std::cout << "AUCTION: bid on " << m_title << " success with " << m_highest.getM_amount();
            return true;
        }
    }

    std::ostream &operator<<(std::ostream &os, const Auction &a) {
        os << "Auction: " << a.m_title << " \tSeller: " << a.m_seller_name << " \tends_at: " << a.m_end
           << "\t m_highest"
           << a.m_highest << "";
        return os;
    }


}