//
// Created by fschl on 5/24/17.
//

#ifndef PV_BOOSTAUCTIONS_AUCTION_H
#define PV_BOOSTAUCTIONS_AUCTION_H

#include <iostream>
//include all types plus i/o
#include "boost/date_time/posix_time/posix_time.hpp"
#include "Bid.h"

namespace asioAuctions {

    class Auction {

    public:
        Auction(std::string title, std::string seller, boost::posix_time::ptime endtime);

        Auction(boost::posix_time::ptime endtime);

    public:

        template<typename Archive>
        void serialize(Archive &ar, const unsigned int version);

        const std::string &getM_seller_name() const;

        void setM_seller_name(const std::string &m_seller_name);

        const std::string &getM_title() const;

        void setM_title(const std::string &m_title);

        const boost::posix_time::ptime &getM_end() const;

        const Bid &getHighest() const;

        const bool &enter_new_bid(const Bid &newOffer);

        friend std::ostream &operator<<(std::ostream &os, const Auction &a);


    private:
        std::string m_seller_name;
        std::string m_title;
        boost::posix_time::ptime m_end;
//        std::vector<Bid> m_bids;
        Bid m_highest;
    };

}

#endif //PV_BOOSTAUCTIONS_AUCTION_H
