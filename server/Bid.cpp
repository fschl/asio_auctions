//
// Created by developer on 5/24/17.
//

#include <boost/date_time/posix_time/posix_time.hpp>
#include "Bid.h"

namespace asioAuctions {

    Bid::Bid(const boost::posix_time::ptime &m_timestamp, const std::string &client, int m_amount) : m_timestamp(
            m_timestamp), m_client(client), m_amount(m_amount) {}

    const boost::posix_time::ptime &Bid::getM_timestamp() const {
        return m_timestamp;
    }

    const std::string &Bid::getClient() const {
        return m_client;
    }

    int Bid::getM_amount() const {
        return m_amount;
    }

    std::ostream &operator<<(std::ostream &os, const Bid &b) {
        os << "Bid: " << b.m_client << " \tAmount: " << b.m_amount << "\tTime: " << boost::posix_time::to_simple_string(b.m_timestamp);
        return os;
    }


}