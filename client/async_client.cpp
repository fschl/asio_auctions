#include <boost/thread.hpp>
#include <boost/bind.hpp>
#include <boost/asio.hpp>
#include <boost/shared_ptr.hpp>
#include <boost/enable_shared_from_this.hpp>
#include <boost/algorithm/string.hpp>

using namespace boost::asio;
io_service service;

// used for async_wait, x = handlerfunc
#define MEM_FN(x)       boost::bind(&self_type::x, shared_from_this())

// used for async_connect, x = endpoint, y = handlerfunc
#define MEM_FN1(x, y)    boost::bind(&self_type::x, shared_from_this(),y)

// used for callback methods on async_read/write. x = handlerfunc, y = error_code, z = number of bytes to read/write
#define MEM_FN2(x, y, z)  boost::bind(&self_type::x, shared_from_this(),y,z)

namespace asioAuctions {

    /** THIS IS THE ACTOR :)
     *
     * simple connection to server:
        - logs in just with username (no password)
        - all connections are initiated by the client: client asks, server answers
        - server disconnects any client that hasn't pinged for 5 seconds

        Possible requests:
        - gets a list of all connected clients
        - ping: the server answers either with "ping ok" or "ping client_list_changed"
    */
    class auctionClient : public boost::enable_shared_from_this<auctionClient>, boost::noncopyable {
        typedef auctionClient self_type;

        auctionClient(const std::string &username, const int highest_bid) : sock_(service), started_(true),
                                                                            username_(username),
                                                                            timer_(service),
                                                                            highest_willing_bid_(highest_bid) {}

        void start(ip::tcp::endpoint ep, const int highest_bid) {
            sock_.async_connect(ep, MEM_FN1(on_connect, _1));
        }

    public:
        typedef boost::system::error_code error_code;
        typedef boost::shared_ptr<auctionClient> ptr;

        static ptr start(ip::tcp::endpoint ep, const std::string &username, const int highest_bid) {
            ptr new_(new auctionClient(username, highest_bid));
            new_->start(ep, highest_bid);
            return new_;
        }

        void stop() {
            if (!started_) return;
            std::cout << "stopping " << username_ << std::endl;
            started_ = false;
            sock_.close();
        }

        bool started() { return started_; }

    private:
        void on_connect(const error_code &err) {
            if (!err) {
                do_write("login " + username_ + "\n");
            } else {
                stop();
            }
        }

        void on_read(const error_code &err, size_t bytes) {
            if (err) stop();
            if (!started()) return;
            // process the msg
            std::string msg(read_buffer_, bytes);
            if (msg.find("login ") == 0) on_login();
            else if (msg.find("ping") == 0) on_ping(msg);
            else if (msg.find("clients ") == 0) on_clients(msg);
            else if (msg.find("auctions ") == 0) on_auctions(msg);
            else if (msg.find("get_auction ") == 0) on_auction_answer(msg);
            else if (msg.find("bid_answer ") == 0) on_bid_answer(msg);
            else std::cerr << username_ << " invalid msg: " << msg << std::endl;
        }

        void on_login() {
            std::cout << username_ << " logged in." << std::endl;
            // do_ask_clients();
            do_ask_auctions();
        }

        void on_ping(const std::string &msg) {
            std::istringstream in(msg);
            std::string answer;
            // check for second word
            in >> answer >> answer;
            if (answer == "client_list_changed") {
                do_ask_clients();
            } else {
                postpone_ping();
            }
        }

        void on_clients(const std::string &msg) {
            std::string clients = msg.substr(8);
            std::cout << username_ << ", new client list: " << clients;
            postpone_ping();
        }

        void on_auctions(const std::string &msg) {
            std::string auctions = msg.substr(9);
            std::vector<std::string> auction_names_vector;
            boost::split(auction_names_vector, auctions, boost::is_any_of(","));
            std::cout << username_ << ", new auction list:" << auctions;

            std::string selected_title = auction_names_vector[rand() % auction_names_vector.size() - 1];

            std::cout << username_ << " asking for " << selected_title << "\n";
            do_ask_auction_by_title(selected_title);
        }

        void on_auction_answer(const std::string &msg) {
            std::string auction = msg.substr(11);
            std::cout << username_ << ", got answer about auction:" << auction;
            postpone_ping();
        }


        void do_ping() {
            do_write("ping\n");
        }

        void postpone_ping() {
            // note: even though the server wants a ping every 5 secs, we randomly
            // don't ping that fast - so that the server will randomly disconnect us
            int millis = rand() % 7000;
            std::cout << username_ << " postponing ping " << millis << " millis" << std::endl;
            timer_.expires_from_now(boost::posix_time::millisec(millis));
            timer_.async_wait(MEM_FN(do_ping));
        }

        void do_ask_clients() {
            do_write("ask_clients\n");
        }

        void do_ask_auctions() {
            do_write("ask_auctions\n");
        }

        void do_ask_auction_by_title(const std::string &title) {
            do_write("get_auction " + title + "\n");
        }

        void do_try_place_bid(const std::string &title) {
            do_write("place_bid on_" + title + " offer_" + std::to_string(highest_willing_bid_) + "\n");
        }

        // ------------------------------
        // H E L P E R  F U N C T I O N S
        void on_write(const error_code &err, size_t bytes) {
            do_read();
        }

        void do_read() {
            async_read(sock_, buffer(read_buffer_), MEM_FN2(read_complete, _1, _2), MEM_FN2(on_read, _1, _2));
        }

        void do_write(const std::string &msg) {
            if (!started()) return;
            std::copy(msg.begin(), msg.end(), write_buffer_);
            sock_.async_write_some(buffer(write_buffer_, msg.size()), MEM_FN2(on_write, _1, _2));
        }

        size_t read_complete(const boost::system::error_code &err, size_t bytes) {
            if (err) return 0;
            bool found = std::find(read_buffer_, read_buffer_ + bytes, '\n') < read_buffer_ + bytes;
            // we read one-by-one until we get to enter, no buffering
            return found ? 0 : 1;
        }

    private:
        ip::tcp::socket sock_;
        enum {
            max_msg = 1024
        };
        char read_buffer_[max_msg];
        char write_buffer_[max_msg];
        bool started_;
        std::string username_;
        deadline_timer timer_;
        std::string subscribed_auction_;
        int highest_willing_bid_;
    };

}

int main(int argc, char *argv[]) {
    // connect several clients
    std::cout << "Starting clients... " << std::endl;

    ip::tcp::endpoint ep(ip::address::from_string("127.0.0.1"), 8001);
    char *names[] = {"John", "James", "Lucy", "Tracy", "Frank", "Abby", 0};
    for (char **name = names; *name; ++name) {
        std::cout << "running auctionClient with " << *name << std::endl;
        asioAuctions::auctionClient::start(ep, *name, (rand() % 300));
        boost::this_thread::sleep(boost::posix_time::millisec(100));
    }

    service.run();
}














