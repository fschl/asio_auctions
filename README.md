# Boost::Asio Auctions

PV Projekt: WHZ C++ Sommer 2017

Example for asynchronous server/client communication using the `boost::asio` library collection.


## Run

The boost development libraries must be installed on the system.

Install on debian-based systems via `sudo apt install libboost-dev-all`

1. compile+start `pv_boostAuctions_server`
2. compile+start `pv_boostAuctions_client`
